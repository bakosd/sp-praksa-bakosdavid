<?php

namespace Drupal\category_import\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure category_import settings for this site.
 */
class SettingsForm extends ConfigFormBase {
  const DEFAULT_FILE_EXTENSION = 'docx doc xml jpg csw png pdf jbg';

  const SETTING_NAME = 'taxonomy_import.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'category_import_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames():array {
    return ['taxonomy_import.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $settingsData = $this->config(static::SETTING_NAME);
    $form['file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed file extensions'),
      '#default_value' => $settingsData->get('file_extensions') ?? self::DEFAULT_FILE_EXTENSION,
      '#required' => TRUE,
    ];

    $form['file_max_size'] = [
      '#type' => 'number',
      '#title' => $this->t('MAX file size (MB)'),
      '#default_value' => $settingsData->get('file_max_size') ?? 5,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ((int) $form_state->getValue('file_max_size') > 1024) {
      $form_state->setErrorByName('file_max_size', $this->t('The MAX allowed file-size limit is 1GB.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::SETTING_NAME)
      ->set('file_extensions', $form_state->getValue('file_extensions'))
      ->set('file_max_size', $form_state->getValue('file_max_size'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
