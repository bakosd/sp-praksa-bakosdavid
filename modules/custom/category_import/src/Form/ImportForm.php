<?php

namespace Drupal\category_import\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\taxonomy\VocabularyStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contribute form.
 */
class ImportForm extends FormBase {
  use StringTranslationTrait;

  /**
   * A config object for the system performance configuration.
   */
  protected Config $config;

  /**
   * Vocabulary storage.
   */
  protected VocabularyStorageInterface $vocabularyStorage;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   Vocabulary storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(
        ConfigFactoryInterface $config_factory,
        VocabularyStorageInterface $vocabulary_storage,
        EntityTypeManagerInterface $entity_type_manager,
        MessengerInterface $messenger,
    ) {
    $this->config = $config_factory->get('taxonomy_import.settings');
    $this->vocabularyStorage = $vocabulary_storage;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ImportForm|static {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'import_taxonomy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $vocabularies = $this->vocabularyStorage->loadMultiple();
    $vocabulariesList = [];

    foreach ($vocabularies as $vid => $vocabulary) {
      $vocabulariesList[$vid] = $vocabulary->get('name');
    }

    $form['field_vocabulary_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary name'),
      '#options' => $vocabulariesList,
      '#attributes' => [
        'class' => ['vocab-name-select'],
      ],
      '#description' => $this->t('Select vocabulary!'),
    ];

    $form['taxonomy_file'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Import file'),
      '#required' => TRUE,
      '#upload_location' => 'public://taxonomy_files/',
      '#upload_validators' => [
        'file_validate_extensions' => [
          $this->config->get('file_extensions') ?? SettingsForm::DEFAULT_FILE_EXTENSION,
        ],
      ],

      '#description' => $this->t('Upload a file to Import taxonomy! (:mbMB)', [':mb' => $this->config->get('file_max_size')]),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state)) {
      $form_state->setErrorByName('You must upload file!');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      if ('field_vocabulary_name' == $key && !empty($value)) {
        try {
          $this->createTaxonomy($value);
        }
        catch (InvalidPluginDefinitionException $e) {
          $this->messenger->addError("Invalid plugin definition.\n" . $e->getMessage());
        }
        catch (PluginNotFoundException $e) {
          $this->messenger->addError("Plugin not found.\n" . $e->getMessage());
        }
        catch (EntityStorageException $e) {
          $this->messenger->addError("Entity storage error.\n" . $e->getMessage());
        }
      }
    }
  }

  /**
   * Function to implement import taxonomy functionality.
   *
   * @param string $voc_name
   *   The name of the vocabulary.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createTaxonomy(string $voc_name) {
    global $base_url;
    $imported_taxonomies = 0;
    $loc = Database::getConnection()->query('SELECT f.uri FROM file_managed f ORDER BY f.fid DESC limit 1', []);
    foreach ($loc as $val) {
      $location = $val->uri;
    }

    $mimetype = function_exists('mime_content_type') ? mime_content_type($location) : 'application/octet-stream';

    // Converting to machine name.
    $machine_readable = strtolower($voc_name);
    // Vocabulary machine name.
    $vid = preg_replace('@[^a-z0-9_]+@', '_', $machine_readable);
    // Creating new vocabulary with the field value.
    $vocabularies = $this->vocabularyStorage->loadMultiple();
    if (!isset($vocabularies[$vid])) {
      $vocabulary = $this->vocabularyStorage->create([
        'vid' => $vid,
        'machine_name' => $vid,
        'name' => $voc_name,
        'description' => '',
      ]);
      $vocabulary->save();
    }
    if ($mimetype != 'text/plain' && $mimetype != 'application/csv') {
      if ('application/octet-stream' == $mimetype) {
        $this->messenger->addError('File contains no data');
        exit();
      }
      $this->messenger->addError('Failed to open the file');
      exit();
    }

    $handle = fopen($location, 'r');
    if ($handle == FALSE) {
      return $this->messenger->addError('File contains no data');
    }

    while (($data = fgetcsv($handle)) !== FALSE) {
      $term_id = $parent_id = 0;
      $terms_id = Database::getConnection()->query(
            'SELECT n.tid FROM taxonomy_term_field_data n WHERE n.name  = :uid AND n.vid  = :vid',
            [':uid' => $data[0], ':vid' => $vid]
        );

      foreach ($terms_id as $val) {
        $term_id = $val->tid;
      }

      if (!empty($data[1])) {
        $parent_ids = Database::getConnection()->query(
              'SELECT n.tid FROM taxonomy_term_field_data n WHERE n.name  = :uid AND n.vid  = :vid',
              [':uid' => $data[1], ':vid' => $vid]
          );
        foreach ($parent_ids as $val) {
          if (!empty($val)) {
            $parent_id = $val->tid;
          }
          else {
            $parent_id = 0;
          }
        }
      }

      $target_term = NULL;
      $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');
      if (empty($term_id)) {
        $term = $termStorage->create([
          'parent' => [$parent_id],
          'name' => $data[0],
          // Sets the description of the term if it's in the CSV.
          'description' => $data[1] ?? '',
          'vid' => $vid,
        ]);
        if ($term->save()) {
          $imported_taxonomies++;
        }

        if ($term->id()) {
          if (!empty($data[0])) {
            $properties['name'] = $data[0];
          }

          if (!empty($vid)) {
            $properties['vid'] = $vid;
          }

          $terms = $termStorage->loadByProperties($properties);
          $term = reset($terms);
          $target_term = $term;
        }
      }
      else {
        $term = $termStorage->load($term_id);
        $term->set('parent', $parent_id);
        $term->save();
        $target_term = $term;
      }

      if (count($data) > 1 && !empty($target_term)) {
        for ($i = 2, $j = 1; $j < count($data); $i++, $j++) {
          if (!empty($data[$i]) && !empty($data[$j])) {
            $target_term->set($data[$j], $data[$i]);
            $target_term->save();
            $imported_taxonomies++;
          }
        }
      }
    }
    fclose($handle);
    if (!empty($imported_taxonomies)) {
      $formatted_url = new FormattableMarkup(
        'Successfully imported :import_count taxonomies to :voc_name. <br></br>Click <a href=":url">here</a> to go to view all taxonomies related to :voc_name.',
        [
          ':voc_name' => ucfirst($voc_name),
          ':import_count' => $imported_taxonomies,
          ':url' => $base_url . '/admin/structure/taxonomy/manage/' . $vid . '/overview',
        ]);
      return $this->messenger->addMessage($formatted_url);
    }
    else {
      return $this->messenger->addWarning("No imported taxonomy.");
    }

  }

}
