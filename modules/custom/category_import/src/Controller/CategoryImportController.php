<?php

namespace Drupal\category_import\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for category_import routes.
 */
class CategoryImportController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build(): array {
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Import all categories'),
    ];

    return $build;
  }

}
