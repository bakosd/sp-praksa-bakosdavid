<?php

namespace Drupal\current_weather_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides a block with openweathermap.org data displayed.
 * @Block(
 *   id = "current_weather_block",
 *   admin_label = @Translation ("Current Weather Block")
 * )
 **/
class WeatherBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $long = "19.653960";
    $lat = "46.094640";
    $city = "Subotica?";

    $default_params = [
      '#theme' => "get-weather-block",
      '#data' => [
        'temp' => "404",
        'icon' => "",
        'city' => $city,
      ],
    ];

    // GuzzleHttp client
    $client = new Client();

    $client_ip = $this->getClientIP();

    if (!empty($client_ip)) {
      $ip_api_result = $client->get("https://api.ipgeolocation.io/ipgeo?apiKey=03dabbc67c5742d9b10f586327687faa&ip=$client_ip&output=json");

      if ($ip_api_result->getStatusCode() == 200)
        $ip_api_result = json_decode($ip_api_result->getBody()->getContents());

      if (!empty($ip_api_result->latitude) && !empty($ip_api_result->longitude) && !empty($ip_api_result->city)) {
        $lat = $ip_api_result->latitude;
        $long = $ip_api_result->longitude;
        $city = $ip_api_result->city;
      }
    }


    try {
      $response = $client->get("https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=7097d7330815cd665239f1809ba1d078&units=metric");
      $response = json_decode($response->getBody()->getContents());
      if (empty($response))
        throw new \Exception("The data was empty.");

      $temperature = round($response->main->temp, 1);
      $icon = $response->weather[0]->icon;

      return [
        '#theme' => "get-weather-block",
        '#data' => [
          'temp' => $temperature,
          'icon' => $icon,
          'city' => $city,
        ],
      ];

    } catch (\Exception | RequestException $exception) {
      error_log("There was an error in Current_Weather_API Block display.\n The error-string: \n" . $exception);
      return $default_params;
    }
  }

  function getClientIP(): ?string
  {
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
      $client_ip = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
      $client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
      $client_ip = $_SERVER['REMOTE_ADDR'];

    if ($client_ip == "::1" || $client_ip == "localhost" || $client_ip == "127.0.0.1")
      return null;

    return $client_ip;
  }
}
