<?php

namespace Drupal\news_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase
{

  /**
   * Default value for the api-key.
   *
   * @var string
   */
  const SETTINGS = 'news_sync.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $settingsData = $this->config(static::SETTINGS);

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $settingsData->get('api_key'),
    ];
    $form['api_default_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API default url (Ex: https://newsapi.org/v2/)'),
      '#default_value' => $settingsData->get('api_default_url'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    if (empty(strlen($form_state->getValue('api_key'))))
      $form_state->setErrorByName('api_key', $this->t('The inputted api key is not in correct format.'));
    if (empty(strlen($form_state->getValue('api_default_url'))))
      $form_state->setErrorByName('api_default_url', $this->t('The inputted api key is not in correct format.'));
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_default_url', $form_state->getValue('api_default_url'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
