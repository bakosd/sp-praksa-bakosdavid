<?php

namespace Drupal\news_sync\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Config;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\node\Entity\Node;
use \Drupal\file\Entity\File;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ContentController extends ControllerBase
{
  protected $httpClient;

  protected $newsSyncSettings;

  protected $fileSystem;

  protected $entity_type_manager;

  public function __construct(ClientInterface $http_client, Config $news_sync_settings, FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->newsSyncSettings = $news_sync_settings;
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory')->get('news_sync.settings'),
      $container->get('file_system'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * @param int $article_count The count of the articles to create.
   * @param string $category The category which is searched. Example: 'business'
   * @param string $search_params The params needs to include the api_key params name at the end! Example: 'top-headlines?country=us&category=business&apiKey='
   * @param string $required_api_base The required base url like "https://newsapi.org/v2/"
   * @return string[]
   */
  public function createArticles(int $article_count, string $category, string $search_params, string $required_api_base){
    $api_key = $this->newsSyncSettings->get('api_key');
    $api_default_url = $this->newsSyncSettings->get('api_default_url');

    if (empty($api_key) || empty($search_params) || empty($api_default_url))
      return $this->displayMessage("Error: Invalid API data could not find one of these: api_key, search_params, api_default_url.");

    if ($required_api_base != $api_default_url)
      return $this->displayMessage("Error: Change the API default url to required! ($required_api_base)");

    $result = null;
    try {
      $result = $this->httpClient->get($api_default_url . $search_params . $api_key);
    } catch (\GuzzleHttp\Exception\ClientException | \Exception $exception) {
      return $this->displayMessage("Error: Bad API response. ErrorMessage: {$exception->getMessage()}");
    }

    if (empty($result))
      return $this->displayMessage("Error: No API result?.");

    $result = json_decode($result->getBody()->getContents());

    $created_articles_count = 0;

    foreach ($result->articles as $article) {
      if ($created_articles_count == $article_count)
        break;

      $article_data = [
        'title' => $article->title,
        'description' => $article->description,
        'body' => $article->content,
        'coverImage' => $article->urlToImage,
        'sourceName' => $article->source->name,
        'author' => $article->author,
        'category' => ucfirst($category),
      ];

      if (str_contains($api_default_url, "newsapi.org")){
        if (empty($article->urlToImage))
          continue;

      } else if (str_contains($api_default_url, "gnews.io")){
        if (empty($article->image))
          continue;

        $article_data['coverImage'] = $article->image;
        $article_data['author'] = "";
      }

      if ($this->createArticleNode($article_data))
        $created_articles_count++;

    }


    return $this->displayMessage("Created $created_articles_count new article from API: '$api_default_url'.");
  }


  protected function createArticleNode($article):bool|int{
    // nodeStorage
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    $news_nodes = $nodeStorage->loadByProperties([
      'type' => 'news',
      'title' => $article['title']
    ]);

    if (!empty($news_nodes))
      return reset($news_nodes)->id();


    $source_node_id = $this->createSourceNode(['title'=>$article['sourceName']]);

    if ($source_node_id) {
      $author = !empty($article['author']) ? strip_tags($article['author']) : "Ismeretlen szerző";

      if(empty($article['coverImage']))
        return false;

      $data = file_get_contents($article['coverImage']);
      $file_name = md5("file-".time()."-".mt_rand(100,999));

      $headers = implode("\n", $http_response_header);
      if (preg_match_all("/^content-type\s*:\s*(.*)$/mi", $headers, $matches)) {
        $file_extension = str_replace('image/', '', end($matches[1]));
      }


      if (empty($file_extension))
        return false;

      $directory = "public://news_covers/";

      $file_system = \Drupal::service('file.repository');

      $this->fileSystem->prepareDirectory($directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);

      $file = $file_system->writeData($data, $directory."$file_name.$file_extension", 1);

      $termStorage = $this->entityTypeManager->getStorage('taxonomy_term');

      $category_terms = $termStorage->loadByProperties([
        'vid' => 'category',
        'name' => $article['category'],
      ]);

      // If term not exists we need to create it.
      if ($category_terms)
        $category_term = reset($category_terms);
      else {
        $category_term = $termStorage->create([
          'vid' => 'category',
          'name' => $article['category'],
        ]);
        $category_term->save();
      }

      $nodeStorage->create([
        'type' => 'news',
        'title' => $article['title'],
        'field_category' => $category_term->id(),
        'body' => $article['body'],
        'field_description' => $article['description'],
        'field_cover_image' => $file->id(),
        'field_news_author' => $author,
        'field_source' =>$source_node_id,
      ])->enforceIsNew()->save();

    }

    return true;
  }


  protected function createSourceNode($source):?int
  {
    $nodeStorage = $this->entityTypeManager->getStorage('node');
    if (!empty($source['title'])) {
      $source_nodes_by_title = $nodeStorage->loadByProperties([
        'type' => 'source',
        'field_title' => $source['title']
      ]);
      $source_node = reset($source_nodes_by_title);
      if (!empty($source_node))
        return $source_node->id();

      $description = !empty($source['description']) ?? "Nincs beállítva leírás ehhez a forráshoz.";

      $source_node = $nodeStorage->create([
        'type' => 'source',
        'title' => $source['title'],
        'field_title' => $source['title'],
        'field_source_description' => $description,
      ]);
      $source_node->save();
      return $source_node->id();

    } else {
      $source_unknown_nodes = $nodeStorage->loadByProperties([
        'type' => 'source',
        'field_title' => "Unknown"
      ]);
      $source_node = reset($source_unknown_nodes);
      if (empty($source_node)) {
        $source_node = $nodeStorage->create([
          'type' => 'source',
          'title' => "Unknown",
          'field_title' => "Unknown",
          'field_source_description' => "The source is unknown.",
        ]);
        $source_node->save();
        return $source_node->id();
      }
    }
    return null;
  }

  private function displayMessage(string $message):array{
    return [
      '#type' => 'markup',
      '#markup' => '<p>' . t($message) . '</p>',
    ];
  }

  public function createArticlesBusiness(){
    return $this->createArticles(10, 'business', 'top-headlines?country=us&category=business&apiKey=', 'https://newsapi.org/v2/');
  }

  public function createArticlesTesla(){
    return $this->createArticles(5, 'tesla', 'everything?q=tesla&from=2023-03-01&sortBy=publishedAt&apiKey=', 'https://newsapi.org/v2/');
  }

  // https://gnews.io/api/v4/top-headlines?category=sports&lang=en&country=us&apikey=a14600a3c15dc988d4c980a51b3580c9
  public function createArticlesOther(){
    return $this->createArticles(5, 'other', 'top-headlines?category=sports&lang=en&country=us&apikey=', 'https://gnews.io/api/v4/');
  }

}
